package com.lara.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/testController/")
public class TestController {

	@RequestMapping(value = "test1", method = RequestMethod.GET)
	public String test1() {
		return "Project Training";
	}
	
	@RequestMapping(value = "test2", method = RequestMethod.POST)
	public List<String> test2() {
		return new ArrayList<String>();
	}

}
